/*
        Write a Java program to print numbers between 1 to 100 which are divisible by 3, 5 and by both.

*/
package com.company;

public class Main {

    public static void main(String[] args) {
        for(int number = 1; number <= 100; number ++ ){

            int three = 3;
            if ((number % three) == 0){
                System.out.print(number + ", ");}
                else {
                System.out.print("");}
        }
        System.out.println();
        System.out.println();
        for(int number2 = 1; number2 <= 100; number2 ++){
            int five = 5;
            if ((number2 % five) == 0){
                System.out.print(number2 + ", ");
            }
        }
        System.out.println();
        System.out.println();
        for(int number3 = 1; number3 <= 100; number3 ++){
          int th = 3;
          int fv = 5;
            if (number3 % th == 0 && number3 % fv == 0){
                System.out.print(number3 + ", ");
            }
        }
    }
}

